﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POS.Web.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectedProduct(int id)
        {
            Product product = ProductRepo.GetById(id);
            OrderDetail detail = new OrderDetail();

            detail.ProductId = product.Id;
            detail.ProductName = product.Name;
            detail.Price = product.Price;

            return PartialView("_SelectedProduct", detail);
        }
    }
}