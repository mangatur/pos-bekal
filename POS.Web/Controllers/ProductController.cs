﻿using POS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POS.Web.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View(ProductRepo.GetAll());
        }

        public ActionResult ProductList()
        {
            return PartialView("_ProductList", ProductRepo.GetAll());
        }
    }
}