/****** Object:  UserDefinedTableType [dbo].[OrderDetailType]    Script Date: 21-Apr-21 02:37:49 PM ******/
CREATE TYPE [dbo].[OrderDetailType] AS TABLE(
	[ProductId] [int] NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Active] [bit] NOT NULL
)
GO
/****** Object:  StoredProcedure [dbo].[sp_MstProductGetAll]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MstProductGetAll] 
@pId int = 0
AS
BEGIN
	SELECT Id, Initial, Name, Description, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate
	FROM MstProduct
	WHERE CASE WHEN @pId = 0 THEN Id ELSE @pId END = Id
	ORDER BY Initial, Name
END

GO
/****** Object:  StoredProcedure [dbo].[sp_MstProductUpdate]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MstProductUpdate] 
@pId Int = 0,
@pInitial Char(10) = '',
@pName VarChar(50) = '',
@pDescription VarChar(500) = '',
@pPrice Decimal(18,2) = 0,
@pActive bit = 0, 
@pCreateBy VarChar(50) = '', 
@pModifyBy VarChar(50) = '',
@pAction TinyInt = 0
-- 0 Insert & Update, 1 Delete
AS
BEGIN
	IF @pAction = 0
		BEGIN
			-- INSERT
			IF @pId = 0
				BEGIN
					INSERT INTO MstProduct 
					(Initial, Name, Description, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
					VALUES 
					(@pInitial, @pName, @pDescription, @pPrice, @pActive, @pCreateBy, GETDATE(), @pModifyBy, GETDATE())
				END
			ELSE
			-- UPDATE
				BEGIN
					UPDATE MstProduct SET Initial = @pInitial, Name = @pName, Description = @pDescription, Price = @pPrice, Active = @pActive, ModifyBy = @pModifyBy, ModifyDate = GETDATE()
					WHERE Id = @pId
				END
		END
	ELSE
		-- DELETE
		BEGIN
			DELETE MstProduct WHERE Id = @pId
		END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_OrderPost]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_OrderPost]
@pReference nchar(15),
@pAmount decimal(18,2) = 0,
@pActive bit = 1, 
@pCreateBy varchar(50) = '',
@pOrderDetails OrderDetailType READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	DECLARE @NewID INT

    BEGIN TRANSACTION;
		
	BEGIN TRY
		INSERT INTO OrderHeader (Reference, Amount, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
		VALUES (@pReference, @pAmount, @pActive, @pCreateBy, GETDATE(), @pCreateBy, GETDATE())

		SET @NewID = CAST(scope_identity() AS int)

		INSERT INTO OrderDetail (HeaderId, ProductId, Quantity, Price, Active, CreateBy, CreateDate, ModifyBy, ModifyDate)
		SELECT @NewID, pOD.ProductId, pOD.Quantity, pOD.Price, 1, @pCreateBy, GETDATE(), @pCreateBy, GETDATE() FROM @pOrderDetails AS pOD

		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION;
	END CATCH;
END

GO
/****** Object:  Table [dbo].[MstProduct]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MstProduct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Initial] [char](10) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Price] [money] NOT NULL,
	[Active] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [varchar](50) NOT NULL,
 CONSTRAINT [PK_MstProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HeaderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifyBy] [varchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderHeader]    Script Date: 21-Apr-21 02:37:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderHeader](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Reference] [nchar](14) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreateBy] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifyBy] [varchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OrderHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MstProduct]  WITH CHECK ADD  CONSTRAINT [FK_MstProduct_MstProduct] FOREIGN KEY([Id])
REFERENCES [dbo].[MstProduct] ([Id])
GO
ALTER TABLE [dbo].[MstProduct] CHECK CONSTRAINT [FK_MstProduct_MstProduct]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_MstProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[MstProduct] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_MstProduct]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_OrderHeader] FOREIGN KEY([HeaderId])
REFERENCES [dbo].[OrderHeader] ([Id])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_OrderHeader]
GO
