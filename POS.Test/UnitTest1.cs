﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using POS.ViewModel;
using System.Collections.Generic;
using POS.Repository;
using System.Diagnostics;

namespace POS.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetAllProduct()
        {
            List<Product> list = ProductRepo.GetAll();
            foreach (var item in list)
            {
                Trace.WriteLine(item.Name);
            }
        }

        [TestMethod]
        public void UpdateProduct()
        {
            Product p = new Product();
            p.Initial = "NASGOR";
            p.Name = "Nasi Goreng";
            p.Description = "Telor dadar";
            p.Price = 22000;
            p.Active = true;

            bool result = ProductRepo.Update(p);
            Trace.Write("Hasilnya adalah:");
            Trace.Write(result);
        }

        [TestMethod]
        public void TestPostOrder()
        {
            OrderHeader oh = new OrderHeader();
            //oh.Reference = "SLS-2104-0001";
            oh.Amount = 275000;
            oh.Active = true;
            oh.CreateBy = "Atur";

            List<OrderDetail> list = new List<OrderDetail>();

            // 1 Kopi Panas, 2 Nasgor
            OrderDetail od = new OrderDetail();
            od.ProductId = 3;
            od.Price = 15000;
            od.Quantity = 3;
            od.Active = true;

            list.Add(od);

            od = new OrderDetail();
            od.ProductId = 4;
            od.Price = 22000;
            od.Quantity = 2;
            od.Active = true;

            list.Add(od);

            oh.List = list;

            bool result = OrderRepo.PostOrder(oh);
            Trace.Write(result);
        }
    }
}
